/* 1 */
select borough_name, count(borough_idBorough)  as occurance_time from 
borough as b join address as a 
on b.idBorough=a.borough_idBorough
group by borough_idBorough
order by borough_idBorough desc
limit 1;

/* 2 */

select borough_name, min(value)
from 
borough join address on borough.idBorough= address.borough_idBorough
join schools on address.idaddress= schools.address_idaddress
join school2crime on schools.idschools= school2crime.schools_idschools
join crimes on school2crime.crimes_idcrimes= crimes.idcrimes
where crimeName like 'nocrime' and type='A'
group by(borough_name);

/* 3 */ 
select max(schools) as maxs, building_name from
address join schools on address.idaddress= schools.address_idaddress
group by building_name
order by maxs desc
limit 1;

/* 4 */
select max(register) as maxr, schools_in_building from 
schools join school2crime on schools.idschools = school2crime.schools_idschools
group by schools_in_building
order by maxr desc
limit 1;

/* 5 */

select max(value) as maxv, schools_in_building from 
crimes join school2crime on crimes.idcrimes= school2crime.crimes_idcrimes
join schools on schools.idschools = school2crime.schools_idschools
where crimeName like 'other' and type='N'
group by schools_idschools
order by maxv desc
limit 1;

/* 6 */

select schools_in_building from
NTA join address on NTA.idnta= address.NTA_idnta
join schools on address.idaddress = schools.address_idaddress

where NTA.nta like 'Chinatown';

/* 7 */

select distinct borough_name from
borough join address on borough.idBorough = address.borough_idBorough
join schools on address.idaddress= schools.address_idaddress
join school2crime on schools.idschools= school2crime.schools_idschools
where schoolYear='2014-15';

/* 8 */ 

select schools_in_building from
schools join school2crime
on schools.idschools= school2crime.schools_idschools
join crimes
on school2crime.crimes_idcrimes=crimes.idcrimes
where crimeName='prop' and type='N'and value>10;


/* 9 */

select distinct location_code from
schools join address on address.idaddress=schools.address_idaddress
where census_tract>10000;

/* 10 */

select distinct location_code from
schools join school2crime on schools.idschools=school2crime.schools_idschools
where register>1000 and register<3000;

/******************************************************************************************************************************************/

select * from crimestoschools;

/* IN */
set @crimename = 'prop';
call schoolsMore10Crims(@crimename);

/* OUT */
 call schoolAmount(@n);
 select @n;

/* INOUT */ 
set @crimetype = 'nocrime';
call crimeTypeSchoolAmount(@crimetype);
select @crimetype;
